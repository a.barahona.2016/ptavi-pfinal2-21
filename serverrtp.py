#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa Servidor RTP
"""

import socketserver
import sys
import json
import socket
import time
from client import TIME
import simplertp

MALFORMED = "400 Bad Request"
BADMETHOD = "405 Method Not Allowed"
OK = "200 OK"

TIME = time.strftime('%Y%m%d%H%M%S ', time.gmtime(time.time() + 3600))


def parse_args():
    correct = True
    if len(sys.argv) != 3:
        correct = False
    else:
        components = sys.argv[1].split(':')
        if len(components) != 2:
            correct = False
        else:
            IPServidorSIP = components[0]
            try:
                puertoServidorSIP = int(components[1])
            except ValueError:
                correct = False
            fichero = sys.argv[2].split('.')
    if not correct:
        sys.exit(
            "Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <file>")
    else:
        return IPServidorSIP, puertoServidorSIP, fichero


class SIPHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    @staticmethod
    def split_adrr(addr):
        correct, user = True, ''
        if not addr.startswith('sip:'):
            correct = False
        components = addr[4:].split('@')
        if len(components) != 2:
            correct = False
        user, _ = components
        return correct, user

    def handle(self):
        msg = self.rfile.read()
        client = self.client_address
        print(TIME + f"SIP from: {client} {msg.decode('utf-8')}")
        components = msg.decode('utf-8')
        components = components.splitlines()
        components = components.pop(0).split()
        if len(components) != 3:
            result = MALFORMED
        else:
            method, addr, proto = components
            if method not in ('INVITE', 'BYE', 'ACK'):
                result = BADMETHOD
                pass
            else:
                correct, user = self.split_adrr(addr)
                if not correct:
                    result = MALFORMED
                else:
                    result = OK
        if (result == OK) and (method == 'ACK'):
            #El rtp sender es un servicio que proporciona 'simplertp.py', se le da como parametros la ip y el puerto al que queremos enviar.
            sender = simplertp.RTPSender('127.0.0.1', 34543, file='cancion.mp3', printout=True)
            sender.send_threaded()
            print("Finalizando el thread de envío.")
            time.sleep(10)
            sender.finish()
        else:
            # Envía la respuesta
            msg = f"SIP/2.0 {result}\r\n"
            self.wfile.write(msg.encode('utf-8'))
            # Al mismo que me lo ha enviado
            print(TIME + f"SIP to: {client} {msg}")


def main():
    IPServidorSIP, puertoServidorSIP, fichero = parse_args()
    print(TIME + 'Starting...')
    #REGISTER#
    with socketserver.UDPServer(('127.0.0.1', 6002), SIPHandler) as serv:
        # Registramos con el socket que usará el servidor SIP
        # register enviará REGISTER a ServidorSIP, y esperará respuesta
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            # Componemos el mensaje
            request = f" REGISTER sip:{fichero[0]}@singasong.net SIP/2.0\n"

            # Enviamos el mensaje
            print(TIME + "SIP to: " + IPServidorSIP + ':' + str(puertoServidorSIP) + request)
            my_socket.sendto(request.encode('utf-8'), (IPServidorSIP, puertoServidorSIP))

            # Recibimos respuesta
            response = my_socket.recv(2048).decode('utf-8')
            lines = response.splitlines()
            first = lines.pop(0)
            if first == "SIP/2.0 200 OK":
                print(TIME + "SIP from: " + IPServidorSIP + ':' + str(puertoServidorSIP) + ' ' + response)
            else:
                print("Error")
            # Ahora ya podemos activar el bucle para recibir peticiones SIP
            # en el mismo socket
            serv.serve_forever()


if __name__ == "__main__":
    main()
