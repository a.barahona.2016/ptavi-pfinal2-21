#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente VoIP
"""


import time
from recv_rtp import RTPHandler
import socketserver
import threading
import socket
import sys

TIME = time.strftime('%Y%m%d%H%M%S ', time.gmtime(time.time() + 3600))

def parse_args():
    correct = True
    if len(sys.argv) != 7:
        correct = False
    else:
        components_Reg = sys.argv[1].split(':')
        if len(components_Reg) != 2:
            correct = False
        else:
            IP_Reg = components_Reg[0]
            try:
                Port_Reg = int(components_Reg[1])
            except ValueError:
                correct = False
            components_Proxy = sys.argv[2].split(':')
            if len(components_Proxy) != 2:
                correct = False
            else:
                IP_Proxy = components_Proxy[0]
                try:
                    Port_Proxy = int(components_Proxy[1])
                except ValueError:
                    correct = False
            Dir_Cliente = sys.argv[3]
            Dir_RTPServer = sys.argv[4]
            timetosleep = sys.argv[5]
            file = sys.argv[6]
    if not correct:
        sys.exit("Usage: python3 client.py <IPReg>:<portReg> \
            <IPProxy>:<portProxy> <addrClient> <addrServerRTP> <time> <file>")
    else:
        return IP_Reg, Port_Reg, IP_Proxy, Port_Proxy, Dir_Cliente, Dir_RTPServer, timetosleep, file


def register():

    IP_Reg, Port_Reg, IP_Proxy, Port_Proxy, Dir_Cliente, Dir_RTPServer, timetosleep, file = parse_args()

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        # Componemos el mensaje
        request = f" REGISTER {Dir_Cliente} SIP/2.0\r\n"

        # Enviamos el mensaje
        print(TIME + "SIP to: " + IP_Reg + ':' + str(Port_Reg) + request)
        my_socket.sendto(request.encode('utf-8'), (IP_Reg, Port_Reg))

        # Recibimos respuesta
        response = my_socket.recv(2048).decode('utf-8')
        # Se queda esperando hasta que llegue un respuesta UDP
        lines = response.splitlines()
        first = lines.pop(0)
        if first == "SIP/2.0 200 OK":
            print(TIME + "SIP from: " + IP_Reg + ':' + str(Port_Reg) + ' ' + response)
        else:
            print("Error")
    return first


def invite():

    IP_Reg, Port_Reg, IP_Proxy, Port_Proxy, Dir_Cliente, Dir_RTPServer, timetosleep, file = parse_args()

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        # Componemos el mensaje
        head = f"Content-Type: application/sdp\r\n"
        body = f"v=0\no=sip:{Dir_Cliente} 127.0.0.1\r\n" f"s={Dir_RTPServer}\nt=0\nm=audio 34543 RTP"
        sdp = f"{head}{body}"
        invite = f" INVITE {Dir_RTPServer} SIP/2.0\r\n{sdp}"

        # Enviamos el mensaje
        print(TIME + "SIP to: " + IP_Proxy + ':' + str(Port_Proxy) + invite)
        my_socket.sendto(invite.encode('utf-8'), (IP_Proxy, Port_Proxy))

        # Recibimos respuesta
        response = my_socket.recv(2048).decode('utf-8')
        # Se queda esperando hasta que llegue un respuesta UDP
        lines = response.splitlines()
        first = lines.pop(0)
        print(TIME + "SIP from: " + IP_Proxy + ':' + str(Port_Proxy) + ' ' + response)
    return first


def bye():

    IP_Reg, Port_Reg, IP_Proxy, Port_Proxy, Dir_Cliente, Dir_RTPServer, timetosleep, file = parse_args()

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        # Componemos el mensaje
        bye = f" BYE {Dir_Cliente} SIP/2.0\r\n"

        # Enviamos el mensaje
        print(TIME + "SIP to: " + IP_Proxy + ':' + str(Port_Proxy) + bye)
        my_socket.sendto(bye.encode('utf-8'), (IP_Proxy, Port_Proxy))

        # Recibimos respuesta
        response = my_socket.recv(2048).decode('utf-8')
        # Se queda esperando hasta que llegue un respuesta UDP
        lines = response.splitlines()
        first = lines.pop(0)
        print(TIME + "SIP from: " + IP_Proxy + ':' + str(Port_Proxy) + ' ' + response)
    return first



def main():
    # Leemos argumentos de línea de comandos
    IP_Reg, Port_Reg, IP_Proxy, Port_Proxy, Dir_Cliente, Dir_RTPServer, timetosleep, file = parse_args()
    # Creamos el socket y lo configuramos
    print(TIME + 'Starting...')
    first = register()
    if first == "SIP/2.0 200 OK":
        first = invite()
        if first == "SIP/2.0 200 OK": ##Enviamos ACK
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                # Componemos el mensaje
                ack = f" ACK {Dir_RTPServer} SIP/2.0\r\n"
                # Enviamos el mensaje
                print(TIME + "SIP to: " + IP_Proxy + ':' + str(Port_Proxy) + ack)
                my_socket.sendto(ack.encode('utf-8'), (IP_Proxy, Port_Proxy))

            RTPHandler.open_output(file)
            with socketserver.UDPServer(('127.0.0.1', 34543), RTPHandler) as serv:
                print("Listening...")
                threading.Thread(
                    target=serv.serve_forever).start()
                time.sleep(int(timetosleep))
                serv.shutdown()
            RTPHandler.close_output()
        else:
            print(TIME + "Error")
        first = bye()
        if first == "SIP/2.0 200 OK":
            pass
        else:
            print(TIME + "Error")
    else:
        print(TIME + "Error")



    print("Terminando programa...")


if __name__ == "__main__":
    main()
