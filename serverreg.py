#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import socket
import time


MALFORMED = "400 Bad Request"
BADMETHOD = "405 Method Not Allowed"
OK = "200 OK"

TIME = time.strftime('%Y%m%d%H%M%S ', time.gmtime(time.time() + 3600))

def parse_args():
    correct = True
    if len(sys.argv) != 3:
        correct = False
    else:
        Port = sys.argv[1]
        try:
            Port = int(Port)
        except ValueError:
            correct = False
        jsonfile = sys.argv[2]
    if not correct:
        sys.exit("Usage: python3 serverreg.py <port> <file>")
    else:
        return Port, jsonfile


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    @staticmethod
    def split_adrr(addr):
        correct, user = True, ''
        if not addr.startswith('sip:'):
            correct = False
        components = addr[4:].split('@')
        if len(components) != 2:
            correct = False
        user,_= components
        return correct, user

    def json2register(self):
        port, jfile = parse_args()
        try:
            with open(jfile, 'r') as jsonfile:
                self.dicc = json.load(jsonfile)
        except:
            pass

    def register2json(self):
        port, jfile = parse_args()
        with open(jfile, 'w') as jsonfile:
            json.dump(self.dicc, jsonfile, indent=4)


    

    def handle(self):
        self.json2register()
        msg = self.rfile.read()
        client = self.client_address
        client_ip = client[0]
        client_port = client[1]

        print(TIME + f"SIP from: {client_ip}:{client_port}{msg.decode('utf-8')}")
        components = msg.decode('utf-8')
        components = components.splitlines()
        components = components.pop(0).split()

        if len(components) != 3:
            result = MALFORMED
        else:
            method, addr, proto = components
            if method not in ('REGISTER'):
                result = BADMETHOD
            else:
                correct, user = self.split_adrr(addr)
                if not correct:
                    result = MALFORMED
                else:
                    result = OK
        if (result == OK) and (method == 'REGISTER'):
            try:
                if self.dicc[addr] != {"address": client}:
                        self.dicc[addr] = {"address": client}
                        self.register2json()
                else:
                    pass
            except:
                self.dicc[addr] = {"address": client}
                self.register2json()

            # Envía la respuesta
            msg = f"SIP/2.0 {result}\r\n"
            self.wfile.write(msg.encode('utf-8'))
            print(TIME + f"SIP to: {client_ip}:{client_port} {msg}")
 

def main():
    # Leemos argumentos de línea de comandos
    ip = "127.0.0.1"
    Port, jsonfile = parse_args()
    ##Creamos servidor y escuchamos

    try:
        serv = socketserver.UDPServer((ip, Port), SIPRegisterHandler)
        print(TIME + "Listening...\n")
    except OSError as e:
        sys.exit(f"Error starting to listen: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Ending server.")
        sys.exit(0)



if __name__ == "__main__":
    main()